# Markdown Cheat Sheet

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

Rubrik 1
#Lalish
Rubrik 2
##Hej på dig
Rubrik 3
###Jag försöker
## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet emphasis i denna mening genom att omge det med `*`.

Betona orden strong emphasis i denna mening genom att omge dem båda med `**`.

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

1.Banan
2.Vindruvor
3.Kiwi
4.Passionsfrukt
5.Mango

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala bananen och skär den i skivor.
2. Skölj vindruvorna och dela dem.
3.Skala kiwi och skär i tärningar.
4.Gröp ur passionsfrukten.
5.Skala mangon och skär i stavar.
6. Blanda alla frukter i en skål.

## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir
ett kodblock.

Skriv 4 blanksteg i början av nedanstående rader för att göra ett kodblock.
        
    x = 3
    y = 5
    if x > y:
    print("x is bigger than y")
    else:
    print("y is bigger than x")

Kodavsnitt kan även göras genom att ange text inom ` tecken.

Denna rad innehåller lite `kod som renderas annorlunda`. Gör ett kodavsnitt av
resten av raden: `x = foo() + bar`

## Länkar

Du kan göra en länk till en annan sida genom att ange texten som ska vara en
länk inom hakparanteser, [ och ]. Efter texten inom hakparanteser ska det finnas
en address till sidan du ska länka till inom vanliga paranteser ( och ).

`En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)`

blir

En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)

Gör ordet Wikipedia i denna mening till en länk till Wikipedias hemsida. [Markdown Syntax](http://www.wikipedia.org/)

## Bilder

En länk till en bild gör du så här:

`![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)`

![Patwic](http://patwic.com/wp-content/uploads/2013/03/elephants_pyramid_1_transparent_web1.png)

Texten inom hakparanteserna är en bildtext och bildens address anges inom
paranteser.

![Parwick](http://www.google.se/search?hl=sv&site=imghp&tbm=isch&source=hp&biw=994&bih=747&q=elefant&oq=elefant&gs_l=img.3..0l10.1429.2599.0.3620.7.5.0.1.1.0.254.886.0j4j1.5.0...0.0...1ac.1.9.img.dwsOIB0xDUU#imgrc=mRGv5bif6cxmpM%3A%3BT8I7xMzVhx8-zM%3Bhttp%253A%252F%252Fupload.wikimedia.org%252Fwikipedia%252Fcommons%252F2%252F22%252FAfrikanischer_Elefant%252C_Miami.jpg%3Bhttp%253A%252F%252Fcommons.wikimedia.org%252Fwiki%252FFile%253AAfrikanischer_Elefant%252C_Miami.jpg%3B2272%3B1704)
